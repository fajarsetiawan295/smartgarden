package com.example.aplikasicabai.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.util.Pair
import com.example.aplikasicabai.LoginActivity
import com.example.aplikasicabai.R
import com.example.aplikasicabai.SplashscreenActivity
import com.example.aplikasicabai.databinding.FragmentStatisticBinding
import com.example.aplikasicabai.formatter.AxisDateFormatter
import com.example.aplikasicabai.model.History
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class StatisticFragment : Fragment() {

    private var _statisticBinding: FragmentStatisticBinding? = null
    private val statisticBinding get() = _statisticBinding!!
    private lateinit var dbRef : DatabaseReference
    private var lineDataset = LineDataSet(null, null)
    private var timeListFormatted: ValueFormatter ?= null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _statisticBinding = FragmentStatisticBinding.inflate(inflater, container,false)
        val sharedPreferences: SharedPreferences? = activity?.getSharedPreferences("user", Context.MODE_PRIVATE)



        with(statisticBinding) {
            btnLogout.setOnClickListener {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("Logout")
                    .setMessage("Apakah Anda ingin keluar?")
                    .setNegativeButton("Tidak") { dialog, which ->
                        dialog.cancel()
                    }
                    .setPositiveButton("Ya") { dialog, which ->
                        val editor = sharedPreferences?.edit()
                        editor?.clear()
                        editor?.apply()
                        Toast.makeText(context, "Berhasil Logout", Toast.LENGTH_SHORT).show()
                        val intent = Intent(context, LoginActivity::class.java)
                        startActivity(intent)
                        activity?.finish()
                    }
                    .show()
            }

        }

        return statisticBinding.root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _statisticBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val myWebView: WebView = view.findViewById(R.id.WebView1)
        myWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                url: String
            ): Boolean {
                view.loadUrl(url)
                return true
            }
        }

        myWebView.loadUrl("https://app-web.sistemwebtest.com")
        myWebView.settings.javaScriptEnabled = true
        myWebView.settings.allowContentAccess = true
        myWebView.settings.domStorageEnabled = true
        myWebView.settings.useWideViewPort = true
    }

}



